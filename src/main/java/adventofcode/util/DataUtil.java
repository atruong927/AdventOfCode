package adventofcode.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * General data utilities.
 */
public class DataUtil {
	/**
	 * Reads the input file.
	 *
	 * @param inputFilePath The input file path.
	 * @return The lines contained in the file.
	 */
	public static List<String> readFileLines(String inputFilePath) {
		List<String> lines = new LinkedList<>();
		try {
			lines = Files.readAllLines(Paths.get(inputFilePath));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lines;
	}

	/**
	 * Converts String list to Integer list.
	 *
	 * @param list The String list.
	 * @return The Integer list.
	 */
	public static List<Integer> convertToIntList(List<String> list) {
		return list.stream().map(Integer::parseInt).collect(Collectors.toList());
	}

	/**
	 * Converts String list to a list of String array.
	 *
	 * @param list  The String list.
	 * @param regex The regex to split each String in the list into a String array.
	 * @return The list of String array.
	 */
	public static List<String[]> convertToStringArrayList(List<String> list, String regex) {
		return list.stream().map(command -> command.split(regex)).collect(Collectors.toList());
	}

	/**
	 * Converts String list to 2D String array.
	 *
	 * @param list  The String list.
	 * @param regex The regex to split each String in the list into a String array.
	 * @return The 2D String array.
	 */
	public static String[][] convertTo2D(List<String> list, String regex) {
		String[][] map = new String[list.size()][list.get(0).length()];

		for (int i = 0; i < list.size(); i++) {
			String[] split = list.get(i).split(regex);
			map[i] = split;
		}

		return map;
	}

	/**
	 * Prints the 2D String array.
	 *
	 * @param map The 2D String array.
	 */
	private static void print(String[][] map) {
		for (int row = 0; row < map.length; row++) {
			for (int column = 0; column < map[row].length; column++) {
				System.out.print(map[row][column]);
			}
			System.out.println();
		}
	}
}
