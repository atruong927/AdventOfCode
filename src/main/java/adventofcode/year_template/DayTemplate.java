package adventofcode.year_template;

import java.util.Arrays;
import java.util.List;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/yyyy/day/#
 *
 * @author Amanda Truong
 */
public class DayTemplate {
	private static final String INPUT_FILE_PATH = "resources/2022/day#-input.txt";

	public static void answer() {
		System.out.println("\nDay #");

		// Load input file
		List<String> exampleLines = Arrays.asList();

		List<String> lines = DataUtil.readFileLines(INPUT_FILE_PATH);

		part1(exampleLines);
		part2(exampleLines);
	}

	private static void part1(List<String> lines) {
		System.out.println("Part 1");
	}

	private static void part2(List<String> lines) {
		System.out.println("Part 2");
	}
}
