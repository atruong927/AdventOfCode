package adventofcode.year_2021;

import java.util.LinkedList;
import java.util.List;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2021/day/2
 *
 * @author Amanda Truong
 */
public class Day2 {
	private static final String INPUT_FILE_PATH = "resources/2021/day2-input.txt";

	public static void answer() {
		System.out.println("\nDay 2");

		List<String> exampleCommands = new LinkedList<>();
		exampleCommands.add("forward 5");
		exampleCommands.add("down 5");
		exampleCommands.add("forward 8");
		exampleCommands.add("up 3");
		exampleCommands.add("down 8");
		exampleCommands.add("forward 2");

		List<String> inputCommands = DataUtil.readFileLines(INPUT_FILE_PATH);

		part1(inputCommands);
		part2(inputCommands);
	}

	private static void part1(List<String> commands) {
		System.out.println("Part 1");

		// split command values
		List<String[]> commandsSplit = DataUtil.convertToStringArrayList(commands, " ");

		// move based on commands
		int horiz = 0;
		int depth = 0;
		for (String[] command : commandsSplit) {
			String course = command[0];
			int value = Integer.parseInt(command[1]);
			if (course.equals("forward")) {
				horiz += value;
			} else if (course.equals("up")) {
				depth -= value;
			} else if (course.equals("down")) {
				depth += value;
			}
		}

		System.out.println("horiz=" + horiz);
		System.out.println("depth=" + depth);

		// find product of horiz and depth
		int product = horiz * depth;
		System.out.println("product=" + product);
	}

	private static void part2(List<String> commands) {
		System.out.println("Part 2");

		// split command values
		List<String[]> commandsSplit = DataUtil.convertToStringArrayList(commands, " ");

		// move + aim based on commands
		int horiz = 0;
		int depth = 0;
		int aim = 0;
		for (String[] command : commandsSplit) {
			String course = command[0];
			int value = Integer.parseInt(command[1]);
			if (course.equals("forward")) {
				horiz += value;
				depth += (aim * value);
			} else if (course.equals("up")) {
				aim -= value;
			} else if (course.equals("down")) {
				aim += value;
			}
		}

		System.out.println("horiz=" + horiz);
		System.out.println("depth=" + depth);

		// find product of horiz and depth
		int product = horiz * depth;
		System.out.println("product=" + product);
	}
}
