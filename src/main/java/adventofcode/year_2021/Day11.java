package adventofcode.year_2021;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2021/day/11
 *
 * @author Amanda Truong
 */
public class Day11 {
	private static final String INPUT_FILE_PATH = "resources/2021/day11-input.txt";

	public static void answer() {
		System.out.println("\nDay 11");

		List<String> exampleLines = new ArrayList<>();
		exampleLines.add("5483143223");
		exampleLines.add("2745854711");
		exampleLines.add("5264556173");
		exampleLines.add("6141336146");
		exampleLines.add("6357385478");
		exampleLines.add("4167524645");
		exampleLines.add("2176841721");
		exampleLines.add("6882881134");
		exampleLines.add("4846848554");
		exampleLines.add("5283751526");

		List<String> inputLines = DataUtil.readFileLines(INPUT_FILE_PATH);

		part1(inputLines, 100);
		part2(inputLines);
	}

	private static void part1(List<String> lines, int steps) {
		System.out.println("Part 1");

		List<List<Integer>> energyLevels = parse(lines);

		// count number of flashes after all steps
		int flashes = 0;
		for (int step = 1; step <= steps; step++) {
			// increase energy levels
			increaseAll(energyLevels);

			// check for flashes
			flashes += checkFlashes(energyLevels);
		}

		System.out.println("flashes=" + flashes);
	}

	private static void part2(List<String> lines) {
		System.out.println("Part 2");

		List<List<Integer>> energyLevels = parse(lines);

		// find total number of octopi
		int numOctopi = energyLevels.size() * energyLevels.get(0).size();

		// find the number of steps when all octopi flash
		int step = 0;
		while (!isAllZero(numOctopi, energyLevels)) {
			step++;

			// increase energy levels
			increaseAll(energyLevels);

			// check for flashes
			checkFlashes(energyLevels);
		}

		System.out.println("step=" + step);
	}

	private static List<List<Integer>> parse(List<String> lines) {
		List<List<String>> energyLevelsString = lines.stream()
				.map(line -> Arrays.asList(line.split("")))
				.collect(Collectors.toList());
		List<List<Integer>> energyLevelsInteger = energyLevelsString.stream()
				.map(list -> list.stream()
						.map(stringValue -> Integer.parseInt(stringValue))
						.collect(Collectors.toList()))
				.collect(Collectors.toList());
		return energyLevelsInteger;
	}

	private static void increaseAll(List<List<Integer>> energyLevels) {
		for (int row = 0; row < energyLevels.size(); row++) {
			for (int column = 0; column < energyLevels.get(row).size(); column++) {
				energyLevels.get(row).set(column, energyLevels.get(row).get(column) + 1);
			}
		}
	}

	private static int checkFlashes(List<List<Integer>> energyLevels) {
		int flashes = 0;
		for (int row = 0; row < energyLevels.size(); row++) {
			for (int column = 0; column < energyLevels.get(row).size(); column++) {
				int energyLevel = energyLevels.get(row).get(column);
				// flash!
				if (energyLevel > 9) {
					flashes++;
					// reset energy level back to zero
					energyLevels.get(row).set(column, 0);
					// increase adjacent energy levels
					int top = row - 1;
					int bottom = row + 1;
					int left = column - 1;
					int right = column + 1;
					increaseAdjacent(top, left, energyLevels);
					increaseAdjacent(top, column, energyLevels);
					increaseAdjacent(top, right, energyLevels);
					increaseAdjacent(row, left, energyLevels);
					increaseAdjacent(row, right, energyLevels);
					increaseAdjacent(bottom, left, energyLevels);
					increaseAdjacent(bottom, column, energyLevels);
					increaseAdjacent(bottom, right, energyLevels);
				}
			}
		}
		if (flashes > 0) {
			flashes += checkFlashes(energyLevels);
		}
		return flashes;
	}

	private static void increaseAdjacent(int row, int column, List<List<Integer>> energyLevels) {
		// dont increase if it aleady flashed (at zero)
		if (row >= 0 && row < energyLevels.size() && column >= 0 && column < energyLevels.get(0).size()) {
			int energyLevel = energyLevels.get(row).get(column);
			if (energyLevel > 0) {
				energyLevels.get(row).set(column, energyLevel + 1);
			}
		}
	}

	private static boolean isAllZero(int numOctopi, List<List<Integer>> energyLevels) {
		int numZero = 0;
		for (List<Integer> list : energyLevels) {
			for (Integer energyLevel : list) {
				if (energyLevel == 0) {
					numZero++;
				}
			}
		}
		return numZero == numOctopi;
	}
}
