package adventofcode.year_2021;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import com.mxgraph.layout.mxCircleLayout;
import com.mxgraph.layout.mxCompactTreeLayout;
import com.mxgraph.layout.mxIGraphLayout;
import com.mxgraph.layout.mxOrganicLayout;
import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.util.mxCellRenderer;

import org.jgrapht.Graph;
import org.jgrapht.ext.JGraphXAdapter;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.traverse.DepthFirstIterator;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2021/day/12
 *
 * @author Amanda Truong
 */
public class Day12 {
	private static final String INPUT_FILE_PATH = "resources/2021/day12-input.txt";

	public static void answer() {
		System.out.println("\nDay 12");

		List<String> exampleLines = new ArrayList<>();
		exampleLines.add("start-A");
		exampleLines.add("start-b");
		exampleLines.add("A-c");
		exampleLines.add("A-b");
		exampleLines.add("b-d");
		exampleLines.add("A-end");
		exampleLines.add("b-end");

		List<String> exampleLines2 = new ArrayList<>();
		exampleLines2.add("dc-end");
		exampleLines2.add("HN-start");
		exampleLines2.add("start-kj");
		exampleLines2.add("dc-start");
		exampleLines2.add("dc-HN");
		exampleLines2.add("LN-dc");
		exampleLines2.add("HN-end");
		exampleLines2.add("kj-sa");
		exampleLines2.add("kj-HN");
		exampleLines2.add("kj-dc");

		List<String> exampleLines3 = new ArrayList<>();
		exampleLines3.add("fs-end");
		exampleLines3.add("he-DX");
		exampleLines3.add("fs-he");
		exampleLines3.add("start-DX");
		exampleLines3.add("pj-DX");
		exampleLines3.add("end-zg");
		exampleLines3.add("zg-sl");
		exampleLines3.add("zg-pj");
		exampleLines3.add("pj-he");
		exampleLines3.add("RW-he");
		exampleLines3.add("fs-DX");
		exampleLines3.add("pj-RW");
		exampleLines3.add("zg-RW");
		exampleLines3.add("start-pj");
		exampleLines3.add("he-WI");
		exampleLines3.add("zg-he");
		exampleLines3.add("pj-fs");
		exampleLines3.add("start-RW");

		List<String> inputLines = DataUtil.readFileLines(INPUT_FILE_PATH);

		part1(exampleLines);
		part2(exampleLines);
	}

	private static void part1(List<String> lines) {
		System.out.println("Part 1");

		System.out.println("lines=" + lines);
		//build graph
		Graph<String, DefaultEdge> graph = build(lines);
		System.out.println("graph=" + graph.toString());
		print(graph);

		// TODO find distinct paths from start to end
		List<String> distinctPaths = new ArrayList<>();
		List<String> path = new LinkedList<>();
		path.add("start");
		for (DefaultEdge edge : graph.outgoingEdgesOf("start")) {
			addChild(edge, graph, path, distinctPaths);
		}

		// TODO find num distinct paths
		System.out.println("distinctPaths=" + distinctPaths);
		System.out.println("num distinct paths=" + distinctPaths.size());
	}

	private static void addChild(DefaultEdge edge, Graph<String, DefaultEdge> graph,
			List<String> path, List<String> distinctPaths) {
		String node = graph.getEdgeTarget(edge);
		long count = path.stream().filter(child -> child.equals(node)).count();
		if(count < 2) {
			path.add(node);
			if(node.equals("end")) {
				distinctPaths.add(path.toString());
				path.remove(node);
			}
			else {
				for (DefaultEdge theEdge : graph.outgoingEdgesOf(node)) {
					addChild(theEdge, graph, path, distinctPaths);
				}
			}
		}
	}

	private static void part2(List<String> lines) {
		System.out.println("Part 2");
	}

	private static Graph<String, DefaultEdge> build(List<String> lines) {
		//find all nodes
		List<String> nodes = new ArrayList<>();
		for (String line : lines) {
			nodes.addAll(Arrays.asList(line.split("-")));
		}
		nodes = nodes.stream()
			.distinct()
			.collect(Collectors.toList());

		//build graph
		Graph<String, DefaultEdge> graph = new SimpleGraph<>(DefaultEdge.class);
		for (String node : nodes) {
			graph.addVertex(node);
		}
		for (String line : lines) {
			String[] split = line.split("-");
			graph.addEdge(split[0], split[1]);
		}

		return graph;
	}

	private static void print(Graph<String, DefaultEdge> graph) {
		JGraphXAdapter<String, DefaultEdge> graphXAdapter = new JGraphXAdapter<>(graph);
		mxIGraphLayout layout = new mxOrganicLayout(graphXAdapter);
		layout.execute(graphXAdapter.getDefaultParent());

		BufferedImage image = mxCellRenderer.createBufferedImage(graphXAdapter, null, 2, Color.WHITE, true, null);
		try {
			ImageIO.write(image, "PNG", Files.newOutputStream(Paths.get("graph.png")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}