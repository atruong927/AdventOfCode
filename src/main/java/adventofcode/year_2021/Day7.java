package adventofcode.year_2021;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2021/day/7
 *
 * @author Amanda Truong
 */
public class Day7 {
	private static final String INPUT_FILE_PATH = "resources/2021/day7-input.txt";

	public static void answer() {
		System.out.println("\nDay 7");

		List<String> exampleLines = Arrays.asList("16,1,2,0,4,2,7,1,2,14");

		List<String> inputLines = DataUtil.readFileLines(INPUT_FILE_PATH);

		part1(inputLines);
		part2(inputLines);
	}

	private static void part1(List<String> lines) {
		System.out.println("Part 1");

		List<Integer> positions = parseLine(lines.get(0));

		int highestPosition = calcHighestPosition(positions);

		// check to see which align position has the lowest total fuel cost
		int leastExpensiveAlignedPosition = Integer.MAX_VALUE;
		int lowestTotalFuelCost = Integer.MAX_VALUE;
		for (int position = 0; position <= highestPosition; position++) {
			int totalFuelCost = calcTotalFuelCostPart1(position, positions);
			if (totalFuelCost < lowestTotalFuelCost) {
				lowestTotalFuelCost = totalFuelCost;
				leastExpensiveAlignedPosition = position;
			}
		}

		System.out.println("alignedPosition=" + leastExpensiveAlignedPosition);
		System.out.println("totalFuelCost=" + lowestTotalFuelCost);
	}

	private static void part2(List<String> lines) {
		System.out.println("Part 2");

		List<Integer> positions = parseLine(lines.get(0));

		int highestPosition = calcHighestPosition(positions);

		// check to see which align position has the lowest total fuel cost
		int leastExpensiveAlignedPosition = Integer.MAX_VALUE;
		int lowestTotalFuelCost = Integer.MAX_VALUE;
		for (int position = 0; position <= highestPosition; position++) {
			int totalFuelCost = calcTotalFuelCostPart2(position, positions);
			if (totalFuelCost < lowestTotalFuelCost) {
				lowestTotalFuelCost = totalFuelCost;
				leastExpensiveAlignedPosition = position;
			}
		}

		System.out.println("alignedPosition=" + leastExpensiveAlignedPosition);
		System.out.println("totalFuelCost=" + lowestTotalFuelCost);
	}

	/**
	 * Parses a line to a list of Integers.
	 *
	 * @param line The line.
	 * @return The list of Integers contained in the line.
	 */
	private static List<Integer> parseLine(String line) {
		return Arrays.asList(line.split(",")).stream()
				.map(position -> Integer.parseInt(position)).collect(Collectors.toList());
	}

	/**
	 * Calculates the highest horizontal position.
	 *
	 * @param positions The list of horizontal positions of each crab.
	 * @return The highest horizontal position.
	 */
	private static int calcHighestPosition(List<Integer> positions) {
		int highestPosition = 0;
		for (Integer horizontalPosition : positions) {
			if (horizontalPosition > highestPosition) {
				highestPosition = horizontalPosition;
			}
		}
		return highestPosition;
	}

	/**
	 * Calculates the total fuel cost to move from horizontal position to aligned
	 * position.
	 *
	 * @param alignedPosition The aligned position.
	 * @param positions       The list of horizontal positions of each crab.
	 * @return The total fuel cost for all crabs.
	 */
	private static int calcTotalFuelCostPart1(int alignedPosition, List<Integer> positions) {
		int totalFuelCost = 0;
		for (Integer position : positions) {
			if (position > alignedPosition) {
				totalFuelCost += position - alignedPosition;
			} else {
				totalFuelCost += alignedPosition - position;
			}
		}
		return totalFuelCost;
	}

	/**
	 * Calculates the total fuel cost to move from horizontal position to aligned
	 * position.
	 *
	 * @param alignedPosition The aligned position.
	 * @param positions       The list of horizontal positions of each crab.
	 * @return The total fuel cost for all crabs.
	 */
	private static int calcTotalFuelCostPart2(int alignedPosition, List<Integer> positions) {
		int totalFuelCost = 0;
		for (Integer position : positions) {
			if (position > alignedPosition) {
				int moves = position - alignedPosition;
				for (int i = 1; i <= moves; i++) {
					totalFuelCost += i;
				}
			} else {
				int moves = alignedPosition - position;
				for (int i = 1; i <= moves; i++) {
					totalFuelCost += i;
				}
			}
		}
		return totalFuelCost;
	}
}
