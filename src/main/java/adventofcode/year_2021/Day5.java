package adventofcode.year_2021;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2021/day/5
 *
 * @author Amanda Truong
 */
public class Day5 {
	private static final String INPUT_FILE_PATH = "resources/2021/day5-input.txt";

	public static void answer() {
		System.out.println("\nDay 5");

		List<String> exampleLines = new LinkedList<>();
		exampleLines.add("0,9 -> 5,9");
		exampleLines.add("8,0 -> 0,8");
		exampleLines.add("9,4 -> 3,4");
		exampleLines.add("2,2 -> 2,1");
		exampleLines.add("7,0 -> 7,4");
		exampleLines.add("6,4 -> 2,0");
		exampleLines.add("0,9 -> 2,9");
		exampleLines.add("3,4 -> 1,4");
		exampleLines.add("0,0 -> 8,8");
		exampleLines.add("5,5 -> 8,2");

		// single examples - part 1
		// exampleLines.add("1,1 -> 1,3");
		// exampleLines.add("9,7 -> 7,7");
		// single examples - part 2
		// exampleLines.add("1,1 -> 3,3");
		// exampleLines.add("9,7 -> 7,9");

		List<String> inputLines = DataUtil.readFileLines(INPUT_FILE_PATH);

		part1(inputLines);
		part2(inputLines);
	}

	private static void part1(List<String> lines) {
		System.out.println("Part 1");

		List<Segment> segments = generateSegments(lines);
		segments.forEach(segment -> segment.setCoords(segment.startCoord, segment.endCoord, false));

		int[][] grid = plotSegmentsOnGrid(segments);

		// count num lines overlapping
		int points = calcPoints(grid);
		System.out.println("points=" + points);
	}

	private static void part2(List<String> lines) {
		System.out.println("\nPart 2");

		List<Segment> segments = generateSegments(lines);
		segments.forEach(segment -> segment.setCoords(segment.startCoord, segment.endCoord, true));

		int[][] grid = plotSegmentsOnGrid(segments);

		// count num lines overlapping
		int points = calcPoints(grid);
		System.out.println("points=" + points);
	}

	/**
	 * Creates a grid and plots the segments' points.
	 *
	 * @param segments The list of segments.
	 * @return The grid with plotted points.
	 */
	private static int[][] plotSegmentsOnGrid(List<Segment> segments) {
		int[][] grid = createGrid(segments);

		// plot segment's coordinates onto grid; if coord is hit, add 1
		for (Segment segment : segments) {
			for (Coordinate coord : segment.coordinates) {
				grid[coord.x][coord.y]++;
			}
		}

		return grid;
	}

	/**
	 * Create empty grid based on list of segments' largest X and Y values.
	 *
	 * @param segments The list of segments.
	 * @return The empty grid.
	 */
	private static int[][] createGrid(List<Segment> segments) {
		int highestX = 0;
		int highestY = 0;
		for (Segment segment : segments) {
			// get highest X
			if (segment.startCoord.x > highestX) {
				highestX = segment.startCoord.x;
			}
			if (segment.endCoord.x > highestX) {
				highestX = segment.endCoord.x;
			}
			// get highest Y
			if (segment.startCoord.y > highestY) {
				highestY = segment.startCoord.y;
			}
			if (segment.endCoord.y > highestY) {
				highestY = segment.endCoord.y;
			}
		}
		return new int[highestX + 1][highestY + 1];
	}

	/**
	 * Calculate overlapping points.
	 *
	 * @param grid The grid.
	 * @return The number of overlapping points.
	 */
	private static int calcPoints(int[][] grid) {
		int points = 0;
		for (int col = 0; col < grid.length; col++) {
			for (int row = 0; row < grid[col].length; row++) {
				if (grid[row][col] > 1) {
					points++;
				}
			}
		}
		return points;
	}

	/**
	 * Prints the grid.
	 *
	 * @param grid The grid.
	 */
	private static void printGrid(int[][] grid) {
		for (int col = 0; col < grid.length; col++) {
			System.out.println();
			for (int row = 0; row < grid[col].length; row++) {
				System.out.print(" " + grid[row][col]);
			}
		}
		System.out.println();
	}

	/**
	 * Generates list of segments from the list of lines.
	 *
	 * @param lines The list of lines.
	 * @return The list of segments.
	 */
	private static List<Segment> generateSegments(List<String> lines) {
		return lines.stream().map(line -> {
			String[] coords = line.split(" -> ");
			String[] coord1 = coords[0].split(",");
			String[] coord2 = coords[1].split(",");

			return new Segment(Integer.parseInt(coord1[0]), Integer.parseInt(coord1[1]), Integer.parseInt(coord2[0]),
					Integer.parseInt(coord2[1]));
		}).collect(Collectors.toList());
	}

	private static class Segment {
		private Coordinate startCoord;
		private Coordinate endCoord;
		private List<Coordinate> coordinates;

		public Segment(int x1, int y1, int x2, int y2) {
			startCoord = new Coordinate(x1, y1);
			endCoord = new Coordinate(x2, y2);
		}

		/**
		 * Sets the list of coordinates making a segment from the start coordinate to
		 * the end coordinate.
		 *
		 * @param startCoord       The start coordinate.
		 * @param endCoord         The end coordinate.
		 * @param includeDiagonals True to include diagonal line to the segment. False
		 *                         to just have horizontal or vertical lines.
		 */
		private void setCoords(Coordinate startCoord, Coordinate endCoord, boolean includeDiagonals) {
			List<Coordinate> coords = new LinkedList<>();
			// create horizontal line
			if (startCoord.y == endCoord.y) {
				// x1 < x2
				if (startCoord.x < endCoord.x) {
					for (int x = startCoord.x; x <= endCoord.x; x++) {
						coords.add(new Coordinate(x, startCoord.y));
					}
				}
				// x1 > x2
				else if (startCoord.x > endCoord.x) {
					for (int x = startCoord.x; x >= endCoord.x; x--) {
						coords.add(new Coordinate(x, startCoord.y));
					}
				}
				// x1 == x2
				else {
					coords.add(startCoord);
				}
			}
			// create vertical line
			if (startCoord.x == endCoord.x) {
				// y1 < y2
				if (startCoord.y < endCoord.y) {
					for (int y = startCoord.y; y <= endCoord.y; y++) {
						coords.add(new Coordinate(startCoord.x, y));
					}
				}
				// y1 > y2
				else if (startCoord.y > endCoord.y) {
					for (int y = startCoord.y; y >= endCoord.y; y--) {
						coords.add(new Coordinate(startCoord.x, y));
					}
				}
				// y1 == y2
				else {
					coords.add(startCoord);
				}
			}
			if (includeDiagonals) {
				// create diagonal line from top left to bottom right
				if (startCoord.x < endCoord.x) {
					// y1 < y2
					if (startCoord.y < endCoord.y) {
						int y = startCoord.y;
						for (int x = startCoord.x; x <= endCoord.x; x++) {
							coords.add(new Coordinate(x, y));
							y++;
						}
					}
					// y1 > y2
					if (startCoord.y > endCoord.y) {
						int y = startCoord.y;
						for (int x = startCoord.x; x <= endCoord.x; x++) {
							coords.add(new Coordinate(x, y));
							y--;
						}
					}
				}

				// create diagonal line from bottom left to top right
				if (startCoord.x > endCoord.x) {
					// y1 < y2
					if (startCoord.y < endCoord.y) {
						int y = startCoord.y;
						for (int x = startCoord.x; x >= endCoord.x; x--) {
							coords.add(new Coordinate(x, y));
							y++;
						}
					}
					// y1 > y2
					if (startCoord.y > endCoord.y) {
						int y = startCoord.y;
						for (int x = startCoord.x; x >= endCoord.x; x--) {
							coords.add(new Coordinate(x, y));
							y--;
						}
					}
				}
			}
			coordinates = coords;
		}

		@Override
		public String toString() {
			return "Segment [startCoord=" + startCoord + ", endCoord=" + endCoord + ", coords=" + coordinates + "]";
		}
	}

	private static class Coordinate {
		private int x;
		private int y;

		public Coordinate(int x, int y) {
			this.x = x;
			this.y = y;
		}

		@Override
		public String toString() {
			return "Coordinate [x=" + x + ", y=" + y + "]";
		}
	}
}
