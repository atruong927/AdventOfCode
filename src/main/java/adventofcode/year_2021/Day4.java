package adventofcode.year_2021;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2021/day/4
 *
 * @author Amanda Truong
 */
public class Day4 {
	private static final String INPUT_FILE_PATH = "resources/2021/day4-input.txt";

	public static void answer() {
		System.out.println("\nDay 4");

		List<String> exampleLines = new LinkedList<>();
		exampleLines.add("7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1");
		exampleLines.add("");
		exampleLines.add("22 13 17 11  0");
		exampleLines.add("8  2 23  4 24");
		exampleLines.add("21  9 14 16  7");
		exampleLines.add("6 10  3 18  5");
		exampleLines.add("1 12 20 15 19");
		exampleLines.add("");
		exampleLines.add("3 15  0  2 22");
		exampleLines.add("9 18 13 17  5");
		exampleLines.add("19  8  7 25 23");
		exampleLines.add("20 11 10 24  4");
		exampleLines.add("14 21 16 12  6");
		exampleLines.add("");
		exampleLines.add("14 21 17 24  4");
		exampleLines.add("10 16 15  9 19");
		exampleLines.add("18  8 23 26 20");
		exampleLines.add("22 11 13  6  5");
		exampleLines.add("2  0 12  3  7");

		List<String> inputLines = DataUtil.readFileLines(INPUT_FILE_PATH);

		part1(inputLines);
		part2(inputLines);
	}

	private static void part1(List<String> lines) {
		System.out.println("Part 1");

		List<Integer> chosenNumbers = generateChosenNumbers(lines);
		List<int[][]> bingoBoards = generateBingoBoards(lines);

		// find winning board
		int winningBoardIndex = -1;
		int winningChosenNumber = -1;
		int chosenNumberCounter = 1;
		for (int chosenNumber : chosenNumbers) {
			for (int i = 0; i < bingoBoards.size(); i++) {
				// update bingo board
				int[][] bingoBoard = bingoBoards.get(i);
				updateBingoBoard(chosenNumber, bingoBoard);

				// check for winners after 5+ chosen numbers
				if (chosenNumberCounter >= 5 && checkWinner(bingoBoard)) {
					winningBoardIndex = i;
					break;
				}
			}

			if (winningBoardIndex >= 0) {
				winningChosenNumber = chosenNumber;
				break;
			}
			chosenNumberCounter++;
		}

		// calculate final score with winning board
		if (winningBoardIndex >= 0) {
			System.out.println("\n*~*~* Winning Board: *~*~*");
			int[][] winningBoard = bingoBoards.get(winningBoardIndex);
			printBingoBoard(winningBoard);
			System.out.println("winningChosenNumber=" + winningChosenNumber);

			// get sum of unmarked values
			int sumUnmarked = 0;
			for (int i = 0; i < 5; i++) {
				for (int j = 0; j < 5; j++) {
					int value = winningBoard[i][j];
					if (value >= 0) {
						sumUnmarked += value;
					}
				}
			}
			System.out.println("\nsum unmarked=" + sumUnmarked);

			// calculate product
			int product = sumUnmarked * winningChosenNumber;
			System.out.println("product=" + product);
		}
	}

	private static void part2(List<String> lines) {
		System.out.println("\nPart 2");

		List<Integer> chosenNumbers = generateChosenNumbers(lines);
		List<int[][]> bingoBoards = generateBingoBoards(lines);

		// find last winning board
		int winningChosenNumber = -1;
		int chosenNumberCounter = 1;
		for (int chosenNumber : chosenNumbers) {
			Iterator<int[][]> iterator = bingoBoards.iterator();
			while (iterator.hasNext()) {
				int[][] bingoBoard = iterator.next();
				updateBingoBoard(chosenNumber, bingoBoard);

				// check for winners after 5+ chosen numbers
				if (chosenNumberCounter >= 5 && checkWinner(bingoBoard)) {
					// if winning board is the last board in the list, break
					if (bingoBoards.size() == 1) {
						winningChosenNumber = chosenNumber;
						break;
					}
					// if winning board isn't the last board in the list, remove it
					else {
						iterator.remove();
					}
				}
			}

			if (winningChosenNumber >= 0) {
				break;
			}
			chosenNumberCounter++;
		}

		// calculate final score with winning board
		if (winningChosenNumber >= 0) {
			System.out.println("\n*~*~* Winning Board: *~*~*");
			int[][] winningBoard = bingoBoards.get(0);
			printBingoBoard(winningBoard);
			System.out.println("winningChosenNumber=" + winningChosenNumber);

			// get sum of unmarked values
			int sumUnmarked = 0;
			for (int i = 0; i < 5; i++) {
				for (int j = 0; j < 5; j++) {
					int value = winningBoard[i][j];
					if (value >= 0) {
						sumUnmarked += value;
					}
				}
			}
			System.out.println("\nsum unmarked=" + sumUnmarked);

			// calculate product
			int product = sumUnmarked * winningChosenNumber;
			System.out.println("product=" + product);
		} else {
			System.out.println("no last winner found");
		}
	}

	/**
	 * Generates the chosen numbers.
	 *
	 * @param lines The list of lines.
	 * @return The chosen numbers, each represented by a list of integers.
	 */
	private static List<Integer> generateChosenNumbers(List<String> lines) {
		return Arrays.asList(lines.get(0).split(",")).stream().map(num -> Integer.parseInt(num))
				.collect(Collectors.toList());
	}

	/**
	 * Generates the bingo boards.
	 *
	 * @param lines The list of lines.
	 * @return The bingo boards, represented
	 */
	private static List<int[][]> generateBingoBoards(List<String> lines) {
		List<int[][]> bingoBoards = new ArrayList<>();
		int[][] bingoBoard = new int[5][5];
		int row = 0;
		for (int i = 2; i < lines.size(); i++) {
			if (!lines.get(i).trim().isEmpty()) {
				// populate bingo board
				int column = 0;
				for (String value : lines.get(i).split(" ")) {
					if (!value.trim().isEmpty()) {
						bingoBoard[row][column] = Integer.parseInt(value.trim());
						column++;
					}
				}
				row++;

				// add bingo board; reset bingo board and row counter
				if (row >= 5) {
					bingoBoards.add(bingoBoard);
					row = 0;
					bingoBoard = new int[5][5];
				}
			}
		}
		return bingoBoards;
	}

	/**
	 * Updates the bingo board, replacing the found chosen number with -1.
	 *
	 * @param chosenNumber The chosen number.
	 * @param bingoBoard   The bingo board.
	 */
	private static void updateBingoBoard(int chosenNumber, int[][] bingoBoard) {
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				if (bingoBoard[i][j] == chosenNumber) {
					bingoBoard[i][j] = -1;
				}
			}
		}
	}

	/**
	 * Checks if the bingo board is a winner!
	 *
	 * @param bingoBoard The bingo board.
	 * @return True if the bingo board is a winner. False is it's a major loser.
	 */
	private static boolean checkWinner(int[][] bingoBoard) {
		int winCounter = 0;

		// check for horizontal win
		if (isHorizWin(bingoBoard)) {
			winCounter++;
		}

		// check for vertical win
		if (isVertWin(bingoBoard)) {
			winCounter++;
		}

		// check for diagonal win (upper left to bottom right)
		// if(isDiagWinUpperLeftToBottomRight(bingoBoard)) {
		// winCounter++;
		// }

		// check for diagonal win (bottom left to upper right)
		// if(isDiagWinBottomLeftToUpperRight(bingoBoard)) {
		// winCounter++;
		// }

		return winCounter > 0;
	}

	/**
	 * Checks if the bingo board has a horizontal win.
	 *
	 * @param bingoBoard The bingo board.
	 * @return True if the bingo board has a horizontal win. False if it's a super
	 *         loser.
	 */
	private static boolean isHorizWin(int[][] bingoBoard) {
		boolean isWin = false;
		for (int row = 0; row < 5; row++) {
			int markCounter = 0;
			for (int column = 0; column < 5; column++) {
				if (bingoBoard[row][column] == -1) {
					markCounter++;
				}
			}
			if (markCounter == 5) {
				isWin = true;
				break;
			}
		}

		return isWin;
	}

	/**
	 * Checks if the bingo board has a vertical win.
	 *
	 * @param bingoBoard The bingo board.
	 * @return True if the bingo board has a vertical win. False if it's a super
	 *         loser.
	 */
	private static boolean isVertWin(int[][] bingoBoard) {
		boolean isWin = false;
		for (int column = 0; column < 5; column++) {
			int markCounter = 0;
			for (int row = 0; row < 5; row++) {
				if (bingoBoard[row][column] == -1) {
					markCounter++;
				}
			}
			if (markCounter == 5) {
				isWin = true;
				break;
			}
		}
		return isWin;
	}

	/**
	 * Checks if the bingo board has a diagonal win from upper left to bottom right.
	 *
	 * @param bingoBoard The bingo board.
	 * @return True if the bingo board has a diagonal win from upper left to bottom
	 *         right. False if it's a super loser.
	 */
	private static boolean isDiagWinUpperLeftToBottomRight(int[][] bingoBoard) {
		int markCounter = 0;
		for (int i = 0; i < 5; i++) {
			if (bingoBoard[i][i] == -1) {
				markCounter++;
			}
		}
		return markCounter == 5;
	}

	/**
	 * Checks if the bingo board has a diagonal win from bottom left to upper right.
	 *
	 * @param bingoBoard The bingo board.
	 * @return True if the bingo board has a diagonal win from bottom left to upper
	 *         right. False if it's a super loser.
	 */
	private static boolean isDiagWinBottomLeftToUpperRight(int[][] bingoBoard) {
		int markCounter = 0;
		for (int row = 0; row < 5; row++) {
			int column = 4 - row;
			if (bingoBoard[row][column] == -1) {
				markCounter++;
			}
		}
		return markCounter == 5;
	}

	/**
	 * Print bingo board.
	 *
	 * @param bingoBoard The bingo board.
	 */
	private static void printBingoBoard(int[][] bingoBoard) {
		for (int i = 0; i < 5; i++) {
			System.out.println();
			for (int j = 0; j < 5; j++) {
				System.out.print(bingoBoard[i][j] + " ");
			}
		}
		System.out.println("\n");
	}
}
