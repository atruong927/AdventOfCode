package adventofcode.year_2021;

import java.util.Arrays;
import java.util.List;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2021/day/1
 *
 * @author Amanda Truong
 */
public class Day1 {
	private static final String INPUT_FILE_PATH = "resources/2021/day1-input.txt";

	public static void answer() {
		System.out.println("\nDay 1");

		// Load input file
		List<Integer> exampleMeasurements = Arrays.asList(199, 200, 208, 210, 200, 207, 240, 269, 260, 263);

		List<String> lines = DataUtil.readFileLines(INPUT_FILE_PATH);
		List<Integer> inputMeasurements = DataUtil.convertToIntList(lines);

		part1(inputMeasurements);
		part2(inputMeasurements);
	}

	private static void part1(List<Integer> measurements) {
		System.out.println("Part 1");

		// Count number of times a depth measurement increases
		int numIncreases = 0;
		for (int i = 1; i < measurements.size(); i++) {
			int current = measurements.get(i);
			int previous = measurements.get(i - 1);
			if (current > previous) {
				numIncreases++;
			}
		}
		System.out.println("num increases = " + numIncreases);
	}

	private static void part2(List<Integer> measurements) {
		System.out.println("Part 2");

		// Count number of times 3-measurement sum window increases
		int numIncreases = 0;
		for (int i = 3; i < measurements.size(); i++) {
			int currentSum = measurements.get(i) + measurements.get(i - 1) + measurements.get(i - 2);
			int previousSum = measurements.get(i - 1) + measurements.get(i - 2) + measurements.get(i - 3);
			if (currentSum > previousSum) {
				numIncreases++;
			}
		}
		System.out.println("num increases = " + numIncreases);
	}
}
