package adventofcode.year_2022;

/**
 * https://adventofcode.com/2022
 *
 * @author Amanda Truong
 */
public class Year2022 {
	public static void answer() {
		System.out.println("\n**2022**");
		// Day1.answer();
		// Day2.answer();
		// Day3.answer();
		// Day4.answer();
		// Day5.answer();
		// Day6.answer();
		Day7.answer();
	}
}
