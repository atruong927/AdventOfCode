package adventofcode.year_2020;

import java.util.Arrays;
import java.util.List;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2020/day/1
 *
 * @author Amanda Truong
 */
public class Day1 {
	private static final String INPUT_FILE_PATH = "resources/2020/day1-input.txt";

	public static void answer() {
		System.out.println("\nDay 1");

		List<Integer> exampleEntries = Arrays.asList(1721, 979, 366, 299, 675, 1456);

		List<String> lines = DataUtil.readFileLines(INPUT_FILE_PATH);
		List<Integer> inputEntries = DataUtil.convertToIntList(lines);

		part1(inputEntries);
		part2(inputEntries);
	}

	private static void part1(List<Integer> entries) {
		System.out.println("Part 1");

		// find 2 entries that sum to 2020
		int entry1 = 0;
		int entry2 = 0;
		for (int i = 0; i < entries.size(); i++) {
			for (int j = 0; j < entries.size(); j++) {
				if (entries.get(i) + entries.get(j) == 2020) {
					entry1 = entries.get(i);
					entry2 = entries.get(j);
					break;
				}
			}
		}

		// multiply the 2 entries and return product
		int product = entry1 * entry2;
		System.out.println("product = " + product);
	}

	private static void part2(List<Integer> entries) {
		System.out.println("Part 2");

		// find 3 entries that sum to 2020
		int entry1 = 0;
		int entry2 = 0;
		int entry3 = 0;
		for (int i = 0; i < entries.size(); i++) {
			for (int j = 0; j < entries.size(); j++) {
				for (int k = 0; k < entries.size(); k++) {
					if (entries.get(i) + entries.get(j) + entries.get(k) == 2020) {
						entry1 = entries.get(i);
						entry2 = entries.get(j);
						entry3 = entries.get(k);
						break;
					}
				}
			}
		}

		// multiply the 3 entries and reutrn product
		int product = entry1 * entry2 * entry3;
		System.out.println("product = " + product);
	}
}
