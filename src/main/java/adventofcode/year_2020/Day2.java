package adventofcode.year_2020;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2020/day/2
 *
 * @author Amanda Truong
 */
public class Day2 {
	private static final String INPUT_FILE_PATH = "resources/2020/day2-input.txt";

	public static void answer() {
		System.out.println("\nDay 2");

		List<String> examplePasswords = new LinkedList<>();
		examplePasswords.add("1-3 a: abcde");
		examplePasswords.add("1-3 b: cdefg");
		examplePasswords.add("2-9 c: ccccccccc");

		List<String> inputPasswords = DataUtil.readFileLines(INPUT_FILE_PATH);

		part1(inputPasswords);
		part2(inputPasswords);
	}

	private static void part1(List<String> passwords) {
		System.out.println("Part 1");

		Long count = passwords.stream()
				.map(inputPassword -> translate(inputPassword))
				.filter(password -> validatePart1(password))
				.count();
		System.out.println("num valid passwords = " + count);
	}

	private static void part2(List<String> passwords) {
		System.out.println("Part 2");

		Long count = passwords.stream()
				.map(inputPassword -> translate(inputPassword))
				.filter(password -> validatePart2(password))
				.count();
		System.out.println("num valid passwords = " + count);
	}

	/**
	 * Breaks down the password into parts.
	 *
	 * @param inputPassword The String password in "[min]-[max] [policy]: [value]"
	 *                      format
	 * @return The Password.
	 */
	private static Password translate(String inputPassword) {
		String[] passwordSplit = inputPassword.split(":");
		String[] policySplit1 = passwordSplit[0].split(" ");
		String[] policySplit2 = policySplit1[0].split("-");

		String policy = policySplit1[1].trim();
		int min = Integer.parseInt(policySplit2[0].trim());
		int max = Integer.parseInt(policySplit2[1].trim());
		String value = passwordSplit[1].trim();

		return new Password(policy, min, max, value);
	}

	/**
	 * Validate the password based on part 1's policy.
	 *
	 * @param password The Password.
	 * @return True if the password is valid. False otherwise.
	 */
	private static boolean validatePart1(Password password) {
		List<String> split = Arrays.asList(password.value.split(""));

		// password must have policy value between min and max number of times
		Long count = split.stream().filter(value -> value.equals(password.policy)).count();

		return count >= password.min && count <= password.max;
	}

	/**
	 * Validate the password based on part 2's policy.
	 *
	 * @param password The Password.
	 * @return True if the password is valid. False otherwise.
	 */
	private static boolean validatePart2(Password password) {
		List<String> split = Arrays.asList(password.value.split(""));
		String val1 = split.get(password.min - 1);
		String val2 = split.get(password.max - 1);

		// policy must occur in only ONE of the values
		return val1.equals(password.policy) ^ val2.equals(password.policy);
	}

	static class Password {
		private String policy;
		private int min;
		private int max;
		private String value;

		public Password(String policy, int min, int max, String value) {
			this.policy = policy;
			this.min = min;
			this.max = max;
			this.value = value;
		}

		@Override
		public String toString() {
			return "Password [min=" + min + ", max=" + max + ", policy=" + policy + ", value=" + value + "]";
		}
	}
}
