package adventofcode.year_2020;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2020/day/4
 *
 * @author Amanda Truong
 */
public class Day4 {
	private static final String INPUT_FILE_PATH = "resources/2020/day4-input.txt";

	/** property/field names */
	private static final String BIRTH_YEAR = "byr";
	private static final String ISSUE_YEAR = "iyr";
	private static final String EXPIRATION_YEAR = "eyr";
	private static final String HEIGHT = "hgt";
	private static final String HAIR_COLOR = "hcl";
	private static final String EYE_COLOR = "ecl";
	private static final String PASSPORT_ID = "pid";
	private static final String COUNTRY_ID = "cid"; // OPTIONAL

	/** required fields */
	private static final List<String> REQUIRED_FIELDS = Arrays.asList(BIRTH_YEAR, ISSUE_YEAR, EXPIRATION_YEAR, HEIGHT,
			HAIR_COLOR, EYE_COLOR, PASSPORT_ID);

	/** regexes */
	private static final String YEAR_REGEX = "^\\d{4}$";
	private static final String HEIGHT_CM_REGEX = "^\\d+cm$";
	private static final String HEIGHT_IN_REGEX = "^\\d+in$";
	private static final String COLOR_REGEX = "^#[a-zA-Z\\d]{6}$";
	private static final List<String> VALID_HAIR_COLORS = Arrays.asList("amb", "blu", "brn", "gry", "grn", "hzl",
			"oth");
	private static final String PASSPORT_REGEX = "^\\d{9}$";

	public static void answer() {
		System.out.println("\nDay 4");

		List<String> exampleLinesPart1 = new LinkedList<>();
		exampleLinesPart1.add("ecl:gry pid:860033327 eyr:2020 hcl:#fffffd");
		exampleLinesPart1.add("byr:1937 iyr:2017 cid:147 hgt:183cm");
		exampleLinesPart1.add("");
		exampleLinesPart1.add("iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884");
		exampleLinesPart1.add("hcl:#cfa07d byr:1929");
		exampleLinesPart1.add("");
		exampleLinesPart1.add("hcl:#ae17e1 iyr:2013");
		exampleLinesPart1.add("eyr:2024");
		exampleLinesPart1.add("ecl:brn pid:760753108 byr:1931");
		exampleLinesPart1.add("hgt:179cm");
		exampleLinesPart1.add("");
		exampleLinesPart1.add("hcl:#cfa07d eyr:2025 pid:166559648");
		exampleLinesPart1.add("iyr:2011 ecl:brn hgt:59in");

		List<String> exampleValidLinesPart2 = new LinkedList<>();
		exampleValidLinesPart2.add("pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980");
		exampleValidLinesPart2.add("hcl:#623a2f");
		exampleValidLinesPart2.add("");
		exampleValidLinesPart2.add("eyr:2029 ecl:blu cid:129 byr:1989");
		exampleValidLinesPart2.add("iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm");
		exampleValidLinesPart2.add("");
		exampleValidLinesPart2.add("hcl:#888785");
		exampleValidLinesPart2.add("hgt:164cm byr:2001 iyr:2015 cid:88");
		exampleValidLinesPart2.add("pid:545766238 ecl:hzl");
		exampleValidLinesPart2.add("eyr:2022");
		exampleValidLinesPart2.add("");
		exampleValidLinesPart2.add("iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719");

		List<String> exampleInvalidLinesPart2 = new LinkedList<>();
		exampleInvalidLinesPart2.add("eyr:1972 cid:100");
		exampleInvalidLinesPart2.add("hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926");
		exampleInvalidLinesPart2.add("");
		exampleInvalidLinesPart2.add("iyr:2019");
		exampleInvalidLinesPart2.add("hcl:#602927 eyr:1967 hgt:170cm");
		exampleInvalidLinesPart2.add("ecl:grn pid:012533040 byr:1946");
		exampleInvalidLinesPart2.add("");
		exampleInvalidLinesPart2.add("hcl:dab227 iyr:2012");
		exampleInvalidLinesPart2.add("ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277");
		exampleInvalidLinesPart2.add("");
		exampleInvalidLinesPart2.add("hgt:59cm ecl:zzz");
		exampleInvalidLinesPart2.add("eyr:2038 hcl:74454a iyr:2023");
		exampleInvalidLinesPart2.add("pid:3556412378 byr:2007");

		List<String> inputLines = DataUtil.readFileLines(INPUT_FILE_PATH);

		List<String> examplePassportsPart1 = combinePassports(exampleLinesPart1);
		List<String> exampleValidPassportsPart2 = combinePassports(exampleValidLinesPart2); // expect 4 valid
		List<String> exampleInvalidPassportsPart2 = combinePassports(exampleInvalidLinesPart2); // expect 0 valid
		List<String> passports = combinePassports(inputLines);
		part1(passports);
		part2(passports);
	}

	/**
	 * Combine lines to create each passport into 1 line.
	 *
	 * @param lines The list of lines.
	 */
	private static List<String> combinePassports(List<String> lines) {
		List<String> passports = new LinkedList<>();
		StringBuilder builder = new StringBuilder();
		Iterator<String> iterator = lines.iterator();
		while (iterator.hasNext()) {
			String line = iterator.next();
			// add passport and reset StringBuilder on empty line
			if (line.trim().isEmpty()) {
				passports.add(builder.toString().trim());
				builder = new StringBuilder();
			}
			// add line to builder
			else {
				builder.append(" ");
				builder.append(line);
			}

			// add passport if last line
			if (!iterator.hasNext() && !builder.toString().isEmpty()) {
				passports.add(builder.toString().trim());
			}
		}

		return passports;
	}

	private static void part1(List<String> passports) {
		System.out.println("Part 1");

		long count = passports.stream().filter(passport -> areRequiredFieldsPresent(passport)).count();
		System.out.println("num valid=" + count);
	}

	private static void part2(List<String> passports) {
		System.out.println("Part 2");

		long count = passports.stream()
				.filter(passport -> areRequiredFieldsValid(passport) && areRequiredFieldsValid(passport)).count();
		System.out.println("num valid=" + count);
	}

	private static boolean areRequiredFieldsPresent(String passport) {
		int fieldCount = 0;
		String[] properties = passport.split(" ");
		for (String property : properties) {
			String[] propertyPair = property.split(":");
			// check if property is present
			if (REQUIRED_FIELDS.contains(propertyPair[0])) {
				fieldCount++;
			}
		}
		return fieldCount >= REQUIRED_FIELDS.size();
	}

	private static boolean areRequiredFieldsValid(String passport) {
		int fieldCount = 0;
		String[] properties = passport.split(" ");
		for (String property : properties) {
			String[] propertyPair = property.split(":");
			String propName = propertyPair[0];
			String propValue = propertyPair[1];
			// check if value is valid
			if (propName.equals(BIRTH_YEAR) && propValue.matches(YEAR_REGEX)) {
				int year = Integer.parseInt(propValue);
				if (year >= 1920 && year <= 2002) {
					fieldCount++;
				}
			} else if (propName.equals(ISSUE_YEAR) && propValue.matches(YEAR_REGEX)) {
				int year = Integer.parseInt(propValue);
				if (year >= 2010 && year <= 2020) {
					fieldCount++;
				}
			} else if (propName.equals(EXPIRATION_YEAR) && propValue.matches(YEAR_REGEX)) {
				int year = Integer.parseInt(propValue);
				if (year >= 2020 && year <= 2030) {
					fieldCount++;
				}
			} else if (propName.equals(HEIGHT)) {
				if (propValue.matches(HEIGHT_CM_REGEX)) {
					int height = Integer.parseInt(propValue.substring(0, propValue.length() - 2));
					if (height >= 150 && height <= 193) {
						fieldCount++;
					}
				}
				if (propValue.matches(HEIGHT_IN_REGEX)) {
					int height = Integer.parseInt(propValue.substring(0, propValue.length() - 2));
					if (height >= 59 && height <= 76) {
						fieldCount++;
					}
				}
			} else if (propName.equals(HAIR_COLOR) && propValue.matches(COLOR_REGEX)) {
				fieldCount++;
			} else if (propName.equals(EYE_COLOR) && VALID_HAIR_COLORS.contains(propValue)) {
				fieldCount++;
			} else if (propName.equals(PASSPORT_ID) && propValue.matches(PASSPORT_REGEX)) {
				fieldCount++;
			}
		}
		return fieldCount >= REQUIRED_FIELDS.size();
	}
}
