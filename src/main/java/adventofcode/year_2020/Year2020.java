package adventofcode.year_2020;

/**
 * https://adventofcode.com/2020
 *
 * @author Amanda Truong
 */
public class Year2020 {
	public static void answer() {
		System.out.println("\n**2020**");
		// Day1.answer();
		// Day2.answer();
		// Day3.answer();
		// Day4.answer();
		// Day5.answer();
		Day6.answer();
	}
}
